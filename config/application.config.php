<?php
$env = getenv('APP_ENV') ?: 'prod'; //if env is set then use that else becomes production by default
return array (
		// This should be an array of module namespaces used in the application.
		'modules' => array (
				'Application' 
		),
		
		// These are various options for the listeners attached to the ModuleManager
		'module_listener_options' => array (
				// This should be an array of paths in which modules reside.
				// If a string key is provided, the listener will consider that a module
				// namespace, the value of that key the specific path to that module's
				// Module class.
				'module_paths' => array (
						'./module',
						'./vendor' 
				),
				
				// An array of paths from which to glob configuration files after
				// modules are loaded. These effectively override configuration
				// provided by modules themselves. Paths may use GLOB_BRACE notation.
				'config_glob_paths' => array (
						 //'config/autoload/{,*.}{global,local}.php', //[AA] default is disabled because we want to use env specific configs
        		sprintf('config/autoload/{,*.}{global,%s,local}.php', $env),
				) 
		
		// Whether or not to enable a configuration cache.
		// If enabled, the merged configuration will be cached and used in
		// subsequent requests.
		// 'config_cache_enabled' => $booleanValue,
				),
		
		

// The key used to create the configuration cache file name.
// 'config_cache_key' => $stringKey,
);