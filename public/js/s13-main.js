(function(w){
	var JSONLoaded = true;
	var B64Loaded = true;
	var jsTagJSON = null;
	var b64Tag = null;
	
	
	if(!w.JSON)
	{
		JSONLoaded = false;
		(function(d){
			 var id = 'socialannex-s13-json3', ref = d.getElementsByTagName('script')[0];
			   if (d.getElementById(id)) {return;}
			   jsTagJSON = d.createElement('script'); jsTagJSON.id = id; jsTagJSON.async = true;
			   jsTagJSON.src = "//cdn.socialannex.com/s13/v2/json3.js";
			   jsTagJSON.onreadystatechange = JSONLoadedAsync;
			   ref.parentNode.insertBefore(jsTagJSON, ref);
		  }(document));
	}
	
	if(!w.btoa)
	{
		B64Loaded = false;
		(function(d){
			 var id = 'socialannex-s13-b64', ref = d.getElementsByTagName('script')[0];
			   if (d.getElementById(id)) {return;}
			   b64Tag = d.createElement('script'); b64Tag.id = id; b64Tag.async = true;
			   b64Tag.src = "//cdn.socialannex.com/s13/v2/base64.js";
			   b64Tag.onreadystatechange = B64LoadedAsync;
			   ref.parentNode.insertBefore(b64Tag, ref);
		  }(document));
	}
	
	function JSONLoadedAsync()
	{
		SAS13Obj.Debug("JSON state :"+jsTagJSON.readyState);
		if((jsTagJSON.readyState == 'loaded' || jsTagJSON.readyState == 'complete'))
		{
			JSONLoaded = true;
			FireS13LoadEvent();
		}	
	}
	
	function B64LoadedAsync()
	{
		SAS13Obj.Debug("B64 state :"+b64Tag.readyState);
		if((b64Tag.readyState == 'loaded' || b64Tag.readyState == 'complete'))
		{
			B64Loaded = true;
			FireS13LoadEvent();
		}
	}
	
	function FireS13LoadEvent()
	{
		if(JSONLoaded && B64Loaded && typeof(w.S13AsyncInit) == 'function')
			w.S13AsyncInit();
		else
			SAS13Obj.Debug("Attempting S13 Init But not all dependencies are loaded or S13AsyncInit is not a function.");
	}
	
	var SAS13Obj = {
			configOptions : {debug:true,debugUserAgent:0},
			SAS13UserAgent : 0,
			SiteID : null,
			RedirectUrl : null,
			CallbackFunction : null,
			SiteUID : null,
			InitOptions : null,
			displayConnectedButtons : false,
			baseUrl: "s13a.socialannex.net/",
			
			detectUserAgent:function ()
			{
				if(navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i))
				{
					this.SAS13UserAgent = 1; //mobile
				}
				else if(navigator.userAgent.match(/MSIE\s[5|6|7|8]{1}/i))
				{
					this.SAS13UserAgent = 3; //IE 8 or less
				}
				else if(navigator.userAgent.match(/MSIE/i))
				{
					this.SAS13UserAgent = 2; //IE 
				}
				else
				{
					this.SAS13UserAgent = 0; //desktop
				}
			},
			Debug : function(msg,type)
			{
				if(this.configOptions.debug && typeof(console) != 'undefined' && window.console)
				{
					if(this.configOptions.debugUserAgent == this.SAS13UserAgent)
					{
						switch(this.SAS13UserAgent)
						{
							case 0:
								if(typeof(console) != 'undefined')
								{
									switch(type)
									{
										case 'warn':
											console.warn(msg);
											break;
										case 'err':
											console.error(msg);
											break;
										case 'network':
											console.error("NetworkError: "+msg.status+" "+msg.statusText+" - "+JSON.parse(msg.responseText).errorMsg);
											break;
										default:
											console.log(msg);
											break;
									}
								}
								break;
							case 1:
								switch(type)
								{
								case 'network':
									alert("NetworkError: "+msg.status+" "+msg.statusText+" - "+JSON.parse(msg.responseText).errorMsg);
									break;
								default:
									alert(msg);
									break;
								}
								break;
							default:
								console.log(msg);
								break;
								
						}
							
					}
				}
			},
			isMobile:function(){
				if(navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i))
				{
					return true;
				}
				return false;
			},
			validateInitParams : function(siteid,callback)
			{
				if(typeof(siteid) != 'number')
					return false;
				if( callback !=null && (typeof(callback) == 'string' || typeof(callback) == 'function'))
					return true;
				
				return false;
			},
			init : function(options,callback)
			{
				if(!this.validateInitParams(options.siteid,callback))
				{
					this.Debug("input params to SAS13Obj.init are invalid",'err');
					return -1;
				}
				
				this.InitOptions = options;
				this.SiteID = this.InitOptions.siteid;
				
				if(typeof(callback) == 'string')
					this.RedirectUrl = callback;
				else if(typeof(callback) == 'function')
					this.CallbackFunction = callback;
				if(this.RedirectUrl != null )
					this.RedirectUrl = this.RedirectUrl.replace(/https?:/i,window.location.protocol); // this is required for cross protocol messaging from server side
				this.SiteUID = this.InitOptions.siteuid;
				this.detectUserAgent();
				tempObj = this;
				
				if(null != this.getUrlParamValue("s13code")){
					var data = {
							oauthToken:this.getUrlParamValue("s13code"),
							siteid:this.SiteID,
							sa_uid:this.getUrlParamValue("s13userid")
					}
					var event = SAS13Obj.btoa(JSON.stringify(data));
					if(this.isMobile() && window.parent.opener == null){
						this.receiveMessage(event);
					}
					else{
						window.parent.opener.SAS13Obj.receiveMessage(event);
						window.parent.close();
						return;
					}
					
				}
				var s13options = null;
				if(window.sessionStorage)
				{
					try {
						s13options = sessionStorage.getItem('s13config:'+encodeURIComponent(window.location.href));
						this.configOptions = JSON.parse(s13options);
					}
					catch(err)
					{
						SAS13Obj.Debug("html5 session storage failure. use fallback");
					}
				}
				
				if(s13options == null){
					this.getScript("//"+this.baseUrl+"application/init/getconfig?siteid="+this.SiteID+"&url="+encodeURIComponent(window.location.href),function(data){
						s13options = JSON.stringify(data);
						tempObj.configOptions = data;
						if(window.sessionStorage)
						{
							try {
								sessionStorage.setItem('s13config:'+encodeURIComponent(window.location.href),s13options);
							}
							catch(err)
							{
								SAS13Obj.Debug("html5 session storage failure. use fallback");
							}
						}
						tempObj.PopulateHtml('show_provider',tempObj.configOptions.largehtml);
						tempObj.PopulateHtml('show_provider_small',tempObj.configOptions.smallhtml);					
					});//end the getScript for get config
				}
				else{
						this.PopulateHtml('show_provider',this.configOptions.largehtml);
						this.PopulateHtml('show_provider_small',this.configOptions.smallhtml);
				}
				
			},
			getUrlParamValue: function (name){
				var queryString = window.location.search.replace("?","");
				var keyVal = queryString.split("&");
				for(var i =0; i< keyVal.length;i++)
				{
					var key = keyVal[i].split("=");
					if(key.length == 2 && key[0] == name)
						return key[1];
				}
				return null;
			},
			PopulateHtml : function(elementID,html)
			{
				var divForBtns  = document.getElementById(elementID);
				if(divForBtns != null)
					divForBtns.innerHTML = html;
				else
					this.Debug("Required div element with id: "+elementID+" Not found on page",'warn');
			},
			createWindowUrl: function(network,redirectUrl){
				var url = "//"+this.baseUrl+"/providers/";
				url += network+".php";
				url += "?siteid="+this.SiteID+"&oauth=true&url="+encodeURIComponent(redirectUrl)+"&pageUrl="+encodeURIComponent(window.location.href);
				return url;
			},
			StartLogin : function(serviceUrl,windowName,serviceName,strWindowFeatures)
			{
				if(this.SAS13UserAgent != 1) //check if we are on desktop
				{
					this.Debug(serviceName+" window options: "+strWindowFeatures);
					if(this.isMobile()){
						window.location = serviceUrl;
					}
					else{
						var win = window.open(serviceUrl,windowName,strWindowFeatures);
						win.focus(); //focus the new open window, because for some providers window looses focus
					}
				}
				else if(this.SAS13UserAgent == 1 ) //check if we are on mobile
				{
					window.location = serviceUrl;// FbLoginConfig.authUrl;
				}
			},
			WindowFeatures: function (width,height)
			{
				var popupWidth = width;
				var popupHeight = height;
				var screenHeight = screen.availHeight/2 -popupHeight/2;
				var screenleft = screen.availWidth/2 - popupWidth/2;
				var strWindowFeatures = "centerscreen=yes,menubar=yes,location=yes,resizable=yes,scrollbars=no,status=yes,height="+popupHeight+",width="+popupWidth+",top="+screenHeight+",left="+screenleft;
				return strWindowFeatures;
			},
			FacebookLogin : function()
			{
				var strWindowFeatures = this.WindowFeatures(400,400);
				var url = this.configOptions.urls.facebook;
				this.StartLogin(url,"sas13_facebook",'facebook',strWindowFeatures);
			},
			//end section facebook login
			GoogleLogin : function()
			{
				this.GooglePlusLogin();
			},
			//end google login
			GooglePlusLogin : function()
			{
				var strWindowFeatures = this.WindowFeatures(500,650);
				var url = this.configOptions.urls.google_plus;
				this.StartLogin(url,"sas13_google_plus",'google_plus',strWindowFeatures);
			},
			//end google plus login
			AmazonLogin : function()
			{
				var strWindowFeatures = this.WindowFeatures(800,550);
				var url = this.configOptions.urls.amazon;
				this.StartLogin(url,"sas13_amazon",'amazon',strWindowFeatures);
			},
			//end amazon login
			YahooLogin : function()
			{
				var strWindowFeatures = this.WindowFeatures(650,500);
				var url = this.configOptions.urls.yahoo;
				this.StartLogin(url,"sas13_yahoo",'yahoo',strWindowFeatures);
			},
			WindowsLiveLogin : function()
			{
				var strWindowFeatures = this.WindowFeatures(500,500);
				var url = this.configOptions.urls.wlive;
				this.StartLogin(url,"sas13_wlive",'wlive',strWindowFeatures);
			},// END
			LinkedinLogin : function()   
			{
				var strWindowFeatures = this.WindowFeatures(500,500);
				var url = this.configOptions.urls.linkedin;
				this.StartLogin(url,"sas13_linkedin",'linkedin',strWindowFeatures);
			}, // END
			PaypalLogin : function()
			{
				var strWindowFeatures = this.WindowFeatures(500,500);
				var url = this.configOptions.urls.paypal;
				this.StartLogin(url,"sas13_paypal",'paypal',strWindowFeatures);
			},
			InstagramLogin : function()   
			{
				var strWindowFeatures = this.WindowFeatures(500,500);
				var url = this.configOptions.urls.instagram;
				this.StartLogin(url,"sas13_instagram",'instagram',strWindowFeatures);
			}, // END
			
			
			btoa : function(str)
			{
				if(w.btoa)
					str = w.btoa(str);
				else if(B64Loaded)
					str = base64.encode(str);
				
				return str;
			},
			atob : function(str)
			{
				if(w.atob)
					str = w.atob(str);
				else if(B64Loaded)
					str = base64.decode(str);
				return str;
			},
			receiveMessage : function(event)
			{
				tempObj = this;
				var msg = this.atob(event);		
				if(msg.length < 1) //this means the cookie is not set and postMessage returned no data
				{
					this.Debug("Communication failure, cannot redirect, Aborting",'err');
					return -1;
				}	
				var msgData = JSON.parse(msg);
				this.Debug("Inter window message recieved");
				if(this.RedirectUrl !=null ){
					var seperator = "?";
					if(/\?/i.test(this.RedirectUrl))
						seperator = "&";
					window.location = this.RedirectUrl+seperator+"code="+msgData.oauthToken;//decodeURIComponent(msgData.Location);
				}
					
				else if(this.CallbackFunction != null)
					this.ProcessCallback(msgData.oauthToken);
				else
					this.Debug("No callbacks defined");
			},
			ProcessCallback : function(token){
				var redirectUrl = window.location.href.replace(/\??\&?code=.*&state=na(#_=_)?/i,"");
				this.Debug("processing callback function: redirect url="+redirectUrl);
				tempObj = this;
				this.getScript("//"+this.baseUrl+"application/auth/getaccesstoken?code="+token+"&siteid="+this.SiteID+"&client_secret="+this.InitOptions.secret,function(data){
					var dataJSON = data;//JSON.parse(data);
					tempObj.getScript("//"+tempObj.baseUrl+"application/auth/getuserinfo?access_token="+dataJSON.access_token,function(userProfileData){
						tempObj.Debug(userProfileData);
						tempObj.CallbackFunction(userProfileData);
					});
				});
			},
			getScript : function(url, callback) {
				var timestamp = new Date().getTime();
				var selfcallback = "s13callback_"+timestamp;
				url = url+"&callback="+selfcallback;
				w[selfcallback] = callback;
				
				var head = document.getElementsByTagName("head")[0];
			    var newScript = document.createElement("script");
			    newScript.type = "text/javascript";
			    newScript.src = url;
			    newScript.async = true;
			    head.appendChild(newScript);
			}
	};
 	
//after the file is loaded fire the events
w.SAS13Obj = SAS13Obj;
FireS13LoadEvent();

})(window);