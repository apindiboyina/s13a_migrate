<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));
//[AA] calculate the environment based on machine hostname
$hostname = gethostname();
if(strpos($hostname,".socialannex.")){
	putenv('ZF2_PATH=/home/socialan/public_html/zend/library/');
	putenv('APP_ENV=prod');
}
else{
	putenv('ZF2_PATH=/var/www/zendframework/trunk/library/');
	putenv('APP_ENV=dev');
}

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();
