<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;
use Zend\Debug\Debug;
use Zend\Http\Response;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR,array($this, 'handleError'));
    }
    
    public function handleError(MvcEvent $evt)
    {
    	//this is to force the error handleing so that our responses are also correct and
    	$js = new JsonModel();
    	$exception = $evt->getParam('exception');
    	if(!$exception){
    		$valueToEncode = array(
    				'error'=>"An error occurred",
    				'message' =>$evt->getError(),
    				'code'=>404,
    				'file'=>$evt->getName(),
    				'line'=>0
    		);
    	}
    	else {
    		$valueToEncode = array(
    				'error'=>"An error occurred",
    				'message' =>$exception->getMessage(),
    				'code'=>$exception->getCode(),
    				'file'=>$exception->getFile(),
    				'line'=>$exception->getLine()
    		);
    	}
    	 
    	$resp = $evt->getResponse();
    	$const = "Zend\Http\Response::STATUS_CODE_" . $valueToEncode['code']; //check if the status code is valid
    	$valueToEncode['code'] = (!defined($const))?"400":$valueToEncode['code']; //if the code is not valid then set it to 400
    	$resp->setStatusCode($valueToEncode['code']);
    	$evt->setResponse($resp);
    	$js->setVariables($valueToEncode);
    	$evt->setViewModel($js);
    }
    

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
