<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Update;
use Zend\Math\Rand;
class SocialUserLoginDetails {
	
	private $_table = 'social_user_login_details';
	private $_adapter;
	private $_tableGateway;
	private $_siteid;
	
	public function __construct($siteid,$adapter){
		$this->_adapter = $adapter;
		$this->_siteid = $siteid;
		$this->_tableGateway = new TableGateway($this->_table, $this->_adapter);
	}
	
	public function UpdateUserInfo(&$profile){ //pass the profile by reference so that any change can reflect on original object
		$row = $this->_tableGateway->select(array('providers'=>$profile['providers'],'site_id'=>$this->_siteid,'loginprovideruid'=>$profile['loginprovideruid']));
		$result = $row->current();
		if($result){
			$profile['num_of_logins'] = $result['num_of_logins'] + 1;
			$this->_tableGateway->update($profile,array('providers'=>$profile['providers'],'site_id'=>$this->_siteid,'loginprovideruid'=>$profile['loginprovideruid']));
			$id = $result['id']; 
		}
		else {
			$date = new \DateTime();
			$profile['db_add_date'] = $date->format(\DateTime::ATOM);
			$profile['uid'] = Rand::getString(10).Rand::getInteger(9999, 999999);
			$this->_tableGateway->insert($profile);
			$id = $this->_tableGateway->getLastInsertValue();
		}
		
		return $id;
	}
	
	public function GetUser($userid){
		$row = $this->_tableGateway->select(array('id'=>$userid));
		return $row->current();
	}
	
	public function GetUserByLoginprovider($provider,$loginprovideruid){
		$row = $this->_tableGateway->select(array('providers'=>$provider,'loginprovideruid'=>$loginprovideruid));
		return $row->current();
	}
	
	public function UpdateByID($userid,$set){
		$this->_tableGateway->update($set,array('id'=>$userid));
	}
}