<?php
namespace Application\Model;

class ImagePaths {
	const CDN_PATH = "//cdn.socialannex.com";
	const FB_LARGE = "/custom_images/s13/fb_large.jpg";
	const FB_SMALL = "/custom_images/s13/fb_small.png";
	const FB_CN	 = "/custom_images/s13/fb_cn.png";
	const GL_LARGE = "/custom_images/s13/gl_large.jpg";
	const GL_SMALL = "/custom_images/s13/gl_small.png";
	const GL_CN = "/custom_images/s13/gl_cn.png";
	const AM_LARGE = "/custom_images/s13/am_large.jpg";
	const AM_SMALL = "/custom_images/s13/am_small.png";
	const AM_CN = "/custom_images/s13/am_cn.png";
	const PY_LARGE = "/custom_images/s13/py_large.png";
	const PY_SMALL = "/custom_images/s13/py_small.png";
	const PY_CN = "/custom_images/s13/py_cn.png";
	const WL_LARGE = "/custom_images/s13/wl_large.png";
	const WL_SMALL = "/custom_images/s13/wl_small.png";
	const WL_CN = "/custom_images/s13/wl_cn.png";
	const YH_LARGE = "/custom_images/s13/yh_large.jpg";
	const YH_SMALL = "/custom_images/s13/yh_small.png";
	const YH_CN = "/custom_images/s13/yh_cn.png";
	const GLP_LARGE = "/custom_images/s13/glp_large.jpg";
	const GLP_SMALL = "/custom_images/s13/glp_small.png";
	const GLP_CN = "/custom_images/s13/glp_cn.png";
	const IN_LARGE = "/custom_images/s13/in_large.png";
	const IN_SMALL = "/custom_images/s13/in_small.png";
	const IN_CN = "/custom_images/s13/in_cn.png";
	const LN_LARGE = "/custom_images/s13/ln_large.png";
	const LN_SMALL = "/custom_images/s13/ln_small.png";
	const LN_CN = "/custom_images/s13/ln_cn.png";
	
	public static function getImagePath($url,$defaultUrl){
		$returnUrl = $url;
		if(empty($url)){
			$returnUrl = self::CDN_PATH.$defaultUrl;
		}
		
		$returnUrl = preg_replace("/https?:/i", "", $returnUrl);
		return $returnUrl;
	}
}