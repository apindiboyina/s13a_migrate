<?php
namespace Application\Model;

use Zend\Serializer\Adapter\Json;
use Zend\Http\Client\Adapter\Curl;
use Zend\Http\Client;
use Zend\Http\Request;
use Application\Model\SocialLoginConfiguration;

class Amazon {
	
	private $_adapter;
	private $_clientID;
	private $_clientSecret;
	private $_redis = null;
	private $_siteid;
	private $_redirectUrl = "https://s13a.socialannex.net/application/amazon/redirect";
		
	public function __construct($siteid,$adapter,$redis=null){
		$this->_adapter = $adapter;
		$this->_siteid = $siteid;
		$this->_redis = $redis;
		$s13config = new S13V2Config($this->_adapter);
		$res = $s13config->getConfig($this->_siteid,$this->_redis);
		$this->_clientID = $res['amazon_client_id'];
		$this->_clientSecret = $res['amazon_client_secret'];
	}
	
	public function getWindowUrl($redirectUrl){
		
		$state = array (
				'siteid' => $this->_siteid,
				'url' => $redirectUrl
		);
		$stateStr = new Json();
		
		$params = array (
				'client_id' => $this->_clientID,
				'redirect_uri' => $this->_redirectUrl,
				'state'=>$stateStr->serialize($state),
				'response_type' => "code",
				'scope' => "profile postal_code"
		);
		$AmzAuthUrl = "https://www.amazon.com/ap/oa?" . http_build_query ( $params );
		return $AmzAuthUrl;
		
	}
	
	public function updateUser($code){
		
		$params = array (
				'code' => $code,
				'client_id' => $this->_clientID,
				'client_secret' => $this->_clientSecret,
				'redirect_uri' => $this->_redirectUrl,
				'grant_type' => "authorization_code"
		);
		$accessTokenUrl = "https://api.amazon.com/auth/o2/token";
		
		$curl = new Client($accessTokenUrl,array(
   		'adapter' => 'Zend\Http\Client\Adapter\Curl'
		));
		$curl->setParameterPost($params);
		$curl->setHeaders(array('Content-type'=>"application/x-www-form-urlencoded"));
		$curl->setMethod("POST");
		$accessToken = $curl->send();
		
		$accessToken = json_decode($accessToken->getBody());
		$params = array (
			'access_token' => $accessToken->access_token
		);
		$profileUrl = "https://api.amazon.com/user/profile";
		
		$curl->reset();
		$curl->setUri($profileUrl);
		$curl->setParameterGet($params);
		$profileResp = $curl->send();
		
		$profile = json_decode ( $profileResp->getBody() );
		//update the database with new information
		$suld = new SocialUserLoginDetails($this->_siteid, $this->_adapter);
		//var_dump($profile);exit();
		$pu = new PersonUnique($this->_siteid, $this->_adapter, $this->_redis);
		$puid = $pu->getPUID($profile->email);
		$date = new \DateTime();
		$name = explode ( " ", $profile->name );
		$values = array(
			'site_id'=>$this->_siteid,
			'providers'=>"amazon",
			'loginprovideruid'=>$profile->user_id,
			'firstname'=>$name[0],
			'lastname'=>$name[1],
			'email'=>$profile->email,
			'zipcode'=>$profile->postal_code,
			'db_update_date'=>$date->format(\DateTime::ATOM),
			'pu_id'=>$puid['pu_id']
		);
		$values['id'] = $suld->UpdateUserInfo($values);
		return $values;
	}
}