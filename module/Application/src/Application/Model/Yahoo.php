<?php
namespace Application\Model;

use Zend\Serializer\Adapter\Json;
use Zend\Http\Client\Adapter\Curl;
use Zend\Http\Client;
use Zend\Http\Request;
use Application\Model\SocialLoginConfiguration;
use Zend\Math\Rand;

class Yahoo {
	
	private $_adapter;
	private $_clientID;
	private $_clientSecret;
	private $_appID;
	private $_redis = null;
	private $_siteid;
	private $_redirectUrl = "http://s13a.socialannex.net/application/yahoo/redirect";
		
	public function __construct($siteid,$adapter,$redis=null){
		$this->_adapter = $adapter;
		$this->_siteid = $siteid;
		$this->_redis = $redis;
		$s13config = new S13V2Config($this->_adapter);
		$res = $s13config->getConfig($this->_siteid,$this->_redis);
		$this->_clientID = $res['yahoo_consumer_key'];
		$this->_clientSecret = $res['yahoo_secret_key'];
		$this->_appID = $res['yahoo_app_id'];
	}
	
	public function getWindowUrl($redirectUrl){
		$state = array (
				'siteid' => $this->_siteid,
				'url' => $redirectUrl
		);
		
		$params = array(
			'siteid'=>$this->_siteid,
			'url'=>$redirectUrl
		);
		$AuthUrl = "http://s13a.socialannex.net/application/yahoo/startlogin?".http_build_query($params);
		return $AuthUrl;
	}
	
	public function GetRequestToken($redirectUrl){
		$state = array (
				'siteid' => $this->_siteid,
				'url' => $redirectUrl
		);
		$nonce = Rand::getString(20);
		$stateStr = new Json();
		$stateStr->serialize($state);
		$timestamp = time();
		$params = array(
				'oauth_nonce'=>$nonce,
				'oauth_timestamp'=>$timestamp,
				'oauth_consumer_key'=>$this->_clientID,
				'oauth_signature_method'=>"plaintext",
				'oauth_signature'=>$this->_clientSecret."&",
				'oauth_version'=>"1.0",
				'xoauth_lang_pref'=>"en-us",
				'oauth_callback'=>$this->_redirectUrl."?state=".$stateStr->serialize($state),
		);
		$AuthUrl = "https://api.login.yahoo.com/oauth/v2/get_request_token";
		$curl = new Client($AuthUrl);
		$curl->setAdapter(new Curl());
		$curl->setParameterGet($params);
		$res = $curl->send();
		$tokens = array(); 
		parse_str($res->getBody(),$tokens);
		//save the oauth token and verifier
		$this->_redis->hset('yahoo_verifier',$tokens['oauth_token'],$tokens['oauth_token_secret']);
		$windowUrl = $tokens['xoauth_request_auth_url'];
		return $windowUrl;
	}
	
	public function updateUser($oauthToken,$oauthVerifier){
		$accessTokenUrl = "https://api.login.yahoo.com/oauth/v2/get_token";
		$tokenSecret = $this->_redis->hget('yahoo_verifier',$oauthToken);
		if($tokenSecret)
			$this->_redis->hdel('yahoo_verifier',$oauthToken); // deletet the secret key as we dont need it anymore
		$params = array(
				'oauth_token'=>$oauthToken,
				'oauth_verifier'=>$oauthVerifier,
				'oauth_nonce'=>Rand::getString(20),
				'oauth_timestamp'=>time(),
				'oauth_consumer_key'=>$this->_clientID,
				'oauth_signature_method'=>"plaintext",
				'oauth_signature'=>$this->_clientSecret."&".$tokenSecret,
				'oauth_version'=>"1.0"
		);
		$curl = new Client($accessTokenUrl,array(
   		'adapter' => 'Zend\Http\Client\Adapter\Curl'
		));
		$curl->setParameterGet($params);
		$accessToken = $curl->send();
		parse_str ( $accessToken->getBody(), $accessToken );
		
		$profileUrl = "https://social.yahooapis.com/v1/user/".$accessToken['xoauth_yahoo_guid']."/profile?";
		$params = array (
				'realm'=>'yahooapis.com',
				'oauth_consumer_key'=>$this->_clientID,
				'oauth_nonce'=>Rand::getString(20),
				'oauth_signature_method'=>'PLAINTEXT',
				'oauth_timestamp'=>time(),
				'oauth_token'=>$accessToken['oauth_token'],
				'oauth_version'=>"1.0",
				'oauth_signature'=>$this->_clientSecret."&".$accessToken['oauth_token_secret'],
				'format'=>"json"
		);
	
		$curl->setUri($profileUrl);
		$curl->setHeaders(array('Authorization'=>"OAuth"));
		$curl->setParameterGet($params);
		$profileResp = $curl->send();
		$profile = json_decode ( $profileResp->getBody() );
		
		//update the database with new information
		$suld = new SocialUserLoginDetails($this->_siteid, $this->_adapter);
		$date = new \DateTime();
		
		$values = array(
			'site_id'=>$this->_siteid,
			'providers'=>"yahoo",
			'loginprovideruid'=>$profile->profile->guid,
			'firstname'=>$profile->profile->givenName,
			'lastname'=>$profile->profile->familyName,
			'image_url'=>$profile->profile->image->imageUrl,
			'profile_url'=>$profile->profile->profileUrl,
			'db_update_date'=>$date->format(\DateTime::ATOM),
			
		);
		
		//check if email received
		if(!$profile->profile->emails[0]){
			//check if an email is already in the system
			$user = $suld->GetUserByLoginprovider('yahoo', $values['loginprovideruid']);
			if($user && !empty($user['email'])){
				$values['email'] = $user['email'];
			}
		}else{
			$values['email'] =$profile->profile->emails[0]->handle;
		}
				
		if($values['email']){
			$pu = new PersonUnique($this->_siteid, $this->_adapter, $this->_redis);
			$puid = $pu->getPUID($values['email']);
			$values['pu_id']=$puid['pu_id'];
		}
		
		$values['id'] = $suld->UpdateUserInfo($values);
		return $values;
	}
}