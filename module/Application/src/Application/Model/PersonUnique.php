<?php
namespace Application\Model;

use Zend\Crypt\PublicKey\Rsa\PrivateKey;
use Zend\Crypt\PublicKey\Rsa\PublicKey;
use Zend\Db\TableGateway\TableGateway;
class PersonUnique{
	private $_table = 'person_unique';
	private $_adapter;
	private $_tableGateway;
	private $_redis = null;
	private $_siteid;
	private static $_result;
	
	public function __construct($siteid,$adapter,$redis){
		$this->_siteid = $siteid;
		$this->_adapter = $adapter;
		$this->_redis = $redis;
		$this->_tableGateway = new TableGateway($this->_table, $this->_adapter);
	}
	
	public function getPUID($email){
		
		if(self::$_result)
			return self::$_result;
		
		if($this->_redis){
			self::$_result = unserialize($this->_redis->hget($this->_table,$email));
			if(self::$_result)
				return self::$_result;
		}
		
		$row = $this->_tableGateway->select(array('social_email_id'=>$email));
		self::$_result = $row->current();
		if(!self::$_result){
			$date = new \DateTime();
			$this->_tableGateway->insert(array('social_email_id'=>$email,'db_add_date'=>$date->format(\DateTime::ATOM)));
			$id = $this->_tableGateway->getLastInsertValue();
			self::$_result = array('pu_id'=>$id,'social_email_id'=>$email,'db_add_date'=>$date->format(\DateTime::ATOM));
		}
		if($this->_redis)
			$this->_redis->hset($this->_table,$email,serialize(self::$_result));
		return self::$_result;		
	}
}