<?php
namespace Application\Model;

use Zend\Serializer\Adapter\Json;
use Zend\Http\Client\Adapter\Curl;
use Zend\Http\Client;
use Zend\Http\Request;
use Application\Model\SocialLoginConfiguration;

class Linkedin {
	
	private $_adapter;
	private $_clientID;
	private $_clientSecret;
	private $_redis = null;
	private $_siteid;
	private $_redirectUrl = "http://s13a.socialannex.net/application/linkedin/redirect";
		
	public function __construct($siteid,$adapter,$redis=null){
		$this->_adapter = $adapter;
		$this->_siteid = $siteid;
		$this->_redis = $redis;
		$s13config = new S13V2Config($this->_adapter);
		$res = $s13config->getConfig($this->_siteid,$this->_redis);
		$this->_clientID = $res['linkedin_api_key'];
		$this->_clientSecret = $res['linkedin_secret_key'];
	}
	
	public function getWindowUrl($redirectUrl){
		
		$state = array (
				'siteid' => $this->_siteid,
				'url' => $redirectUrl
		);
		$stateStr = new Json();
		
		$params = array (
				'client_id' => $this->_clientID,
				'redirect_uri' => $this->_redirectUrl,
				'state'=>$stateStr->serialize($state),
				'response_type' => "code",
				'scope' => 'r_basicprofile r_fullprofile r_emailaddress r_contactinfo'
		);
		$lnUrl = "https://www.linkedin.com/uas/oauth2/authorization?" . http_build_query ( $params );
    return $lnUrl;
	}
	
	public function updateUser($code){
		
		$params = array (
				'code' => $code,
				'client_id' => $this->_clientID,
				'client_secret' => $this->_clientSecret,
				'redirect_uri' => $this->_redirectUrl,
				'grant_type' => "authorization_code"
		);
		$accessTokenUrl = 'https://www.linkedin.com/uas/oauth2/accessToken';
		
		$curl = new Client($accessTokenUrl,array(
   		'adapter' => 'Zend\Http\Client\Adapter\Curl'
		));
		$curl->setParameterGet($params);
		$accessToken = $curl->send();
		
		$accessToken = json_decode($accessToken->getBody());
		$params = array (
			'oauth2_access_token' => $accessToken->access_token,
      'format' => 'json'
		);
		$profileUrl = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,location,picture-url,email-address,date-of-birth,main-address,public-profile-url,interests,languages,following,phoneNumbers,positions)";
		$profileResp = file_get_contents($profileUrl."?".http_build_query($params));
		$profile = json_decode ($profileResp);
		//update the database with new information
		$suld = new SocialUserLoginDetails($this->_siteid, $this->_adapter);
		$pu = new PersonUnique($this->_siteid, $this->_adapter, $this->_redis);
		$puid = $pu->getPUID($profile->emailAddress);
		$date = new \DateTime();
		$values = array(
			'site_id'=>$this->_siteid,
			'providers'=>"linkedin",
			'loginprovideruid'=>$profile->id,
			'firstname'=>$profile->firstName,
			'lastname'=>$profile->lastName,
			'image_url'=>($profile->pictureUrl)?$profile->pictureUrl:"",
			'profile_url' => $profile->publicProfileUrl,
			'email'=>$profile->emailAddress,
			'address'=>$profile->mainAddress,
			'city'=>$profile->location->name,
			'db_update_date'=>$date->format(\DateTime::ATOM),
			'pu_id'=>$puid['pu_id']
		);
		$values['id'] = $suld->UpdateUserInfo($values);
		return $values;
	}
}