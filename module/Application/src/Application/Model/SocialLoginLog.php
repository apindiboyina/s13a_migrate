<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
class SocialLoginLog {
	
	public static function Log($adapter,$siteid,$profile){
		$table = 'social_login_log';
		$tg = new TableGateway($table, $adapter);
		$date = new \DateTime();
		$loginType = 1; //this is for new accounts
		if($profile['num_of_logins'] > 0)
			$loginType = 2; // this is for repeat login
		$values = array(
			'site_id'=>$siteid,
			'log_date'=>$date->format(\DateTime::ATOM),
			'login_type'=>$loginType,
			'provider'=>$profile['providers'],
			'pu_id'=>$profile['pu_id']
		);
		$tg->insert($values);
	}
}