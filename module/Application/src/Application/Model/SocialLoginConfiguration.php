<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
class SocialLoginConfiguration {
	private $_table = 'social_login_configuration';
	private $_tableGateway;
	private $_adapter;
	
	public function __construct($adapter){
		$this->_adapter = $adapter;
		$this->_tableGateway = new TableGateway($this->_table, $this->_adapter);
	}
	
	public function getConfig($siteid){
		$rowset = $this->_tableGateway->select(array('site_id'=>$siteid));
		return $rowset->current();
	}
	
	public function providersEnable($siteid){
		$res = $this->getConfig($siteid);
		$data = array(
			'facebook'=>boolval($res['facebook']),
			'google'=>boolval($res['google']),
			'google_plus'=>boolval($res['google_plus']),
			'yahoo'=>boolval($res['yahoo']),
			'amazon'=>boolval($res['amazon']),
			'paypal'=>boolval($res['paypal']),
			'linkedin'=>boolval($res['linkedin']),
			'instagram'=>boolval($res['instagram']),
			'windows_live'=>boolval($res['windows_live'])
		);
		return $data;
	}
	
	public function getSecretForOldLogin($siteid){
		$res = $this->getConfig($siteid);
		return $res['security_access_token'];
	}
}