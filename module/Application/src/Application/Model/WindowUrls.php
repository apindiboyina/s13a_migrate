<?php
namespace Application\Model;

use Zend\Crypt\PublicKey\Rsa\PublicKey;
class WindowUrls {
	
	private $_siteid;
	private $_adapter;
	
	public function __construct($siteid , $adapter){
		$this->_adapter = $adapter;
		$this->_siteid = $siteid;	
	}
	public function getWindowUrls($enabledProviders,$clientRedirectUrl){
		$data = array();
		if($enabledProviders['facebook']){
			$fb = new Facebook($this->_siteid, $this->_adapter);
			$data['facebook']=$fb->getWindowUrl($clientRedirectUrl);
		}
		
		if($enabledProviders['google_plus'] || $enabledProviders['google']){
			$gp = new GooglePlus($this->_siteid, $this->_adapter);
			$data['google_plus']=$gp->getWindowUrl($clientRedirectUrl);
		}
		
		if($enabledProviders['amazon']){
			$gp = new Amazon($this->_siteid, $this->_adapter);
			$data['amazon']=$gp->getWindowUrl($clientRedirectUrl);
		}
		
		if($enabledProviders['paypal']){
			$pp = new Paypal($this->_siteid, $this->_adapter);
			$data['paypal']=$pp->getWindowUrl($clientRedirectUrl);
		}
		
		if($enabledProviders['instagram']){
			$in = new Instagram($this->_siteid, $this->_adapter);
			$data['instagram']=$in->getWindowUrl($clientRedirectUrl);
		}
		if($enabledProviders['linkedin']){
			$ln = new Linkedin($this->_siteid, $this->_adapter);
			$data['linkedin']=$ln->getWindowUrl($clientRedirectUrl);
		}
		
		if($enabledProviders['yahoo']){
			$yh = new Yahoo($this->_siteid, $this->_adapter);
			$data['yahoo']=$yh->getWindowUrl($clientRedirectUrl);
		}
		
		return $data;
	}
}