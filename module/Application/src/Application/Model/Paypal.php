<?php
namespace Application\Model;

use Zend\Serializer\Adapter\Json;
use Zend\Http\Client\Adapter\Curl;
use Zend\Http\Client;
use Zend\Http\Request;
use Application\Model\SocialLoginConfiguration;

class Paypal {
	
	private $_adapter;
	private $_clientID;
	private $_clientSecret;
	private $_redis = null;
	private $_siteid;
	private $_redirectUrl = "https://s13a.socialannex.net/application/paypal/redirect";
	private $_paypalHost = "https://www.sandbox.paypal.com";
		
	public function __construct($siteid,$adapter,$redis=null){
		$this->_adapter = $adapter;
		$this->_siteid = $siteid;
		$this->_redis = $redis;
		$s13config = new S13V2Config($this->_adapter);
		$res = $s13config->getConfig($this->_siteid,$this->_redis);
		$this->_clientID = $res['paypal_client_id'];
		$this->_clientSecret = $res['paypal_secret_key'];
	}
	
	public function getWindowUrl($redirectUrl){
		
		$nonce = time () . rand ();
		$state = array (
				'siteid' => $this->_siteid,
				'url' => $redirectUrl
		);
		$stateStr = new Json();
		
		$params = array (
				'client_id' => $this->_clientID,
				'redirect_uri' => $this->_redirectUrl,
				'state'=>$stateStr->serialize($state),
				'response_type' => "code",
				'scope' => "openid profile email address phone https://uri.paypal.com/services/paypalattributes",
				'nonce' => $nonce
		);
		$paypalAuthUrl =  $this->_paypalHost."/webapps/auth/protocol/openidconnect/v1/authorize?" . http_build_query ( $params );
    return $paypalAuthUrl;
		
	}
	
	public function updateUser($code){
		
		$params = array (
				'code' => $code,
				'client_id' => $this->_clientID,
				'client_secret' => $this->_clientSecret,
				'redirect_uri' => $this->_redirectUrl,
				'grant_type' => "authorization_code"
		);
		$accessTokenUrl =  $this->_paypalHost."/webapps/auth/protocol/openidconnect/v1/tokenservice";
		
		$curl = new Client($accessTokenUrl,array(
   		'adapter' => 'Zend\Http\Client\Adapter\Curl'
		));
		$curl->setParameterPost($params);
		$curl->setHeaders(array('Content-type'=>"application/x-www-form-urlencoded"));
		$curl->setMethod("POST");
		$accessToken = $curl->send();
		
		$accessToken = json_decode($accessToken->getBody());
		$params = array (
			'schema' => 'openid',
			'access_token' => $accessToken->access_token
		);
		$profileUrl =  $this->_paypalHost."/webapps/auth/protocol/openidconnect/v1/userinfo";
		
		$curl->reset();
		$curl->setUri($profileUrl);
		$curl->setParameterGet($params);
		$profileResp = $curl->send();
		
		$profile = json_decode ( $profileResp->getBody() );
		//update the database with new information
		$suld = new SocialUserLoginDetails($this->_siteid, $this->_adapter);
		//var_dump($profile);exit();
		$pu = new PersonUnique($this->_siteid, $this->_adapter, $this->_redis);
		$puid = $pu->getPUID($profile->email);
		$date = new \DateTime();
		$values = array(
			'site_id'=>$this->_siteid,
			'providers'=>"paypal",
			'loginprovideruid'=>$profile->user_id,
			'firstname' => $profile->given_name,
      'lastname' => $profile->family_name,
			'email'=>$profile->email,
			'address'=>$profile->address->street_address,
			'city'=>$profile->address->locality,
			'state'=>$profile->address->region,
			'country'=>$profile->address->country,
			'zipcode'=>$profile->address->postal_code,
			'phone_number'=>$profile->phone_number,
			'db_update_date'=>$date->format(\DateTime::ATOM),
			'pu_id'=>$puid['pu_id']
		);
		$values['id'] = $suld->UpdateUserInfo($values);
		return $values;
	}
}