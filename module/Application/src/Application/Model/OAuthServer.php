<?php
namespace Application\Model;

use Zend\Math\Rand;
class OAuthServer{
	
	private $_siteid;
	private $_storage = null;
	
	public function __construct($siteid,$storage){
		
		if(!$storage)
			throw new \Exception("storage not provided to OAuthServer", 500);
		
		$this->_siteid = $siteid;
		$this->_storage = $storage;
	}
	
	public function GenerateAccessToken($userid){
		
		$key = md5($userid.Rand::getString(10));
		$this->_storage->set($key,$userid,3600);
		return $key;
	}
	
	public function VerifyAccessToken($accessToken){
		$res = $this->_storage->get($accessToken);
		return $res;
	}
	
	public function GenerateOauthCode($userid){
		$key = md5($userid.Rand::getString(10));
		$this->_storage->hset($this->_siteid,$key,$userid);
		return $key;
	}
	
	public function VerifyOauthCode($code){
		$res = $this->_storage->hget($this->_siteid,$code);
		if($res)
			$this->_storage->hdel($this->_siteid,$code); //oauthcode is one time use code
		return $res;
	}
}