<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Http\Client;
use Zend\Http\Client\Adapter\Curl;
class EmailVerification {
	private $_siteid = null;
	private $_email = null;
	private $_table = "s13_verify_email";
	private $_adapter = null;
	private $_tableGateway = null;
	
	public function __construct($siteid,$adapter)
	{
		$this->_siteid = $siteid;
		$this->_adapter = $adapter;
		$this->_tableGateway = new TableGateway($this->_table, $this->_adapter);
	}
	
	public function SendVerificationEmail($email) {
		$code = rand(99999, 999999);
		$data = array(
				'code'=>$code,
				'email'=>$email,
				'siteid'=>$this->_siteid
		);
		$sql = $this->_tableGateway->insert($data);
		return json_decode($this->sendEmail($this->_siteid, $email, $code),true);
	}
	
	private function sendEmail($siteid,$email, $code)
	{
		$first_name= "Verification Email" ;
		$last_name= "";
		$set_from = 'no-reply@sociallogin.com';
		//set parameter
		$params = array(
				'api_user'=> "socialannex",
				'api_key' => "sendgrid123456",
				'to' 			=> $email,
				'subject' =>"Verify your email address to login.",
				'html'    => "Hello, <br/> Here is your verification code, ".$code." please use this to complete login, at Social Login.",
				'from'    =>$set_from,
				'fromname'=>$first_name." ".$last_name,
		);
		$request = "http://sendgrid.com/api/mail.send.json";
		$curl = new Client($request);
		$curl->setAdapter(new Curl());
		$curl->setParameterPost($params);
		$curl->setHeaders(array('Content-type'=>'application/x-www-form-urlencoded'));
		$curl->setMethod("POST");
		$res = $curl->send();
		return $res->getBody();
	}
	
	public function CheckCode($code,$email) {
		
		$res = $this->_tableGateway->select(array('siteid'=>$this->_siteid,'code'=>$code,'email'=>$email));
		if($res->current()){
			$this->_tableGateway->delete(array('siteid'=>$this->_siteid,'code'=>$code,'email'=>$email));
			return true;
		}
		return false;
	}
}