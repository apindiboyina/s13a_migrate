<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
class S13V2Config {

	private $_table = 's13_v2_config';
	private $_tableGateway;
	private $_adapter;
	private static $_result = null;
	
	public function __construct($adapter){
		$this->_adapter = $adapter;
		$this->_tableGateway = new TableGateway($this->_table, $this->_adapter);
	}
	
	public function getConfig($siteid,$redis = NULL){
		//get from memory
		if(self::$_result)
			return self::$_result;
		
		//get from cache
		if($redis){
			self::$_result = unserialize($redis->hget($siteid,$this->_table));
			if(self::$_result)
				return self::$_result;
		}
		
		//get from db
		$rowset = $this->_tableGateway->select(array('siteid'=>$siteid));
		self::$_result = $rowset->current();
		if($redis){
			$redis->hset($siteid,$this->_table,serialize(self::$_result));
		}
		return self::$_result;
	}
}