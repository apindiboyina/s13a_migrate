<?php
namespace Application\Model;

use Zend\Serializer\Adapter\Json;
use Zend\Http\Client\Adapter\Curl;
use Zend\Http\Client;
use Zend\Http\Request;
use Application\Model\SocialLoginConfiguration;

class GooglePlus {
	
	private $_adapter;
	private $_clientID;
	private $_clientSecret;
	private $_redis = null;
	private $_siteid;
	private $_redirectUrl = "http://s13a.socialannex.net/application/googleplus/redirect";
		
	public function __construct($siteid,$adapter,$redis=null){
		$this->_adapter = $adapter;
		$this->_siteid = $siteid;
		$this->_redis = $redis;
		$s13config = new S13V2Config($this->_adapter);
		$res = $s13config->getConfig($this->_siteid,$this->_redis);
		$this->_clientID = $res['google_plus_client_id'];
		$this->_clientSecret = $res['google_plus_secret_key'];
	}
	
	public function getWindowUrl($redirectUrl){
		
		$state = array (
				'siteid' => $this->_siteid,
				'url' => $redirectUrl
		);
		$stateStr = new Json();
		
		$params = array (
				'client_id' => $this->_clientID,
				'redirect_uri' => $this->_redirectUrl,
				'state'=>$stateStr->serialize($state),
				'response_type' => "code",
				'scope' => "openid email profile https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.profile"
		);
		$googleAuthUrl = "https://accounts.google.com/o/oauth2/auth?" . http_build_query ( $params );
		return $googleAuthUrl;
	}
	
	public function updateUser($code){
		
		$params = array (
				'code' => $code,
				'client_id' => $this->_clientID,
				'client_secret' => $this->_clientSecret,
				'redirect_uri' => $this->_redirectUrl,
				'grant_type' => "authorization_code"
		);
		$accessTokenUrl = "https://accounts.google.com/o/oauth2/token";
		
		$curl = new Client($accessTokenUrl,array(
   		'adapter' => 'Zend\Http\Client\Adapter\Curl'
		));
		$curl->setParameterPost($params);
		$curl->setHeaders(array('Content-type'=>"application/x-www-form-urlencoded"));
		$curl->setMethod("POST");
		$accessToken = $curl->send();
		
		$accessToken = json_decode($accessToken->getContent());
		$params = array (
			'access_token' => $accessToken->access_token
		);
		$profileUrl = "https://www.googleapis.com/oauth2/v3/userinfo";
		
		$curl->reset();
		$curl->setUri($profileUrl);
		$curl->setParameterGet($params);
		$profileResp = $curl->send();
		
		$profile = json_decode ( $profileResp->getBody() );
		//update the database with new information
		$suld = new SocialUserLoginDetails($this->_siteid, $this->_adapter);
		
		$pu = new PersonUnique($this->_siteid, $this->_adapter, $this->_redis);
		$puid = $pu->getPUID($profile->email);
		$date = new \DateTime();
		$values = array(
			'site_id'=>$this->_siteid,
			'providers'=>"google_plus",
			'loginprovideruid'=>$profile->sub,
			'firstname'=>$profile->given_name,
			'lastname'=>$profile->family_name,
			'email'=>$profile->email,
			'profile_url'=>$profile->profile,
			'image_url'=>$profile->picture,
			'db_update_date'=>$date->format(\DateTime::ATOM),
			'pu_id'=>$puid['pu_id']
		);
		$values['id'] = $suld->UpdateUserInfo($values);
		return $values;
	}
}