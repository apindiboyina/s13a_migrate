<?php
namespace Application\Model;

use Zend\Serializer\Adapter\Json;
use Zend\Http\Client\Adapter\Curl;
use Zend\Http\Client;
use Zend\Http\Request;
use Application\Model\SocialLoginConfiguration;

class Facebook {
	
	private $_adapter;
	private $_clientID;
	private $_clientSecret;
	private $_redis = null;
	private $_siteid;
	private $_redirectUrl = "http://s13a.socialannex.net/application/facebook/redirect";
		
	public function __construct($siteid,$adapter,$redis=null){
		$this->_adapter = $adapter;
		$this->_siteid = $siteid;
		$this->_redis = $redis;
		$s13config = new S13V2Config($this->_adapter);
		$res = $s13config->getConfig($this->_siteid,$this->_redis);
		$this->_clientID = $res['fb_app_id'];
		$this->_clientSecret = $res['fb_app_secret'];
	}
	
	public function getWindowUrl($redirectUrl){
		$state = array (
				'siteid' => $this->_siteid,
				'url' => $redirectUrl
		);
		$stateStr = new Json();
		$stateStr->serialize($state);
		$params = array(
			'display'=>"popup",
			'client_id'=>$this->_clientID,
			'redirect_uri'=>$this->_redirectUrl,
			'scope'=>"email,user_birthday,user_location",
			'state'=>$stateStr->serialize($state),
			'response_type'=>"code"
		);
		$facbookAuthUrl = "https://www.facebook.com/dialog/oauth?".http_build_query($params);
		return $facbookAuthUrl;
	}
	
	public function updateUser($code){
		$accessTokenUrl = "https://graph.facebook.com/oauth/access_token";
		$params = array (
				'client_id' => $this->_clientID,
				'redirect_uri' => $this->_redirectUrl,
				'client_secret' => $this->_clientSecret,
				'code' => $code
		);
		$curl = new Client($accessTokenUrl,array(
   		'adapter' => 'Zend\Http\Client\Adapter\Curl'
		));
		$curl->setParameterGet($params);
		$accessToken = $curl->send();
		parse_str ( $accessToken->getContent(), $accessToken );
		$profileUrl = "https://graph.facebook.com/me";
		$params = array (
				'access_token' => $accessToken['access_token']
		);
		$curl->setUri($profileUrl);
		$curl->setParameterGet($params);
		$profileResp = $curl->send();
		$profile = json_decode ( $profileResp->getContent() );
		
		//update the database with new information
		$suld = new SocialUserLoginDetails($this->_siteid, $this->_adapter);
		$date = new \DateTime();
		$birthday = \DateTime::createFromFormat("m/d/Y", $profile->birthday);
		$age = 0;
		if(null != $birthday)
			$age = $date->diff($birthday)->y;
		
		$values = array(
			'site_id'=>$this->_siteid,
			'providers'=>"facebook",
			'loginprovideruid'=>$profile->id,
			'firstname'=>$profile->first_name,
			'lastname'=>$profile->last_name,
			'birth_date'=>(null !=$birthday)?$birthday->format(\DateTime::ATOM):'0000-00-00',
			'age'=>$age,
			'profile_url'=>$profile->link,
			'db_update_date'=>$date->format(\DateTime::ATOM),
		);
		//var_dump($values);exit();
		//check if email received
		if(!$profile->email){
			//check if an email is already in the system
			$user = $suld->GetUserByLoginprovider('facebook', $values['loginprovideruid']);
			if($user && !empty($user['email'])){
				$values['email'] = $user['email'];
			}
		}else{
			$values['email'] = $profile->email;
		}
				
		if($values['email']){
			$pu = new PersonUnique($this->_siteid, $this->_adapter, $this->_redis);
			$puid = $pu->getPUID($values['email']);
			$values['pu_id']=$puid['pu_id'];
		}
		
		$values['id'] = $suld->UpdateUserInfo($values);
		return $values;
	}
}