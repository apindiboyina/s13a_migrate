<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Platform\Platform;
use Zend\Db\Adapter\Platform\Sql92;
class S4V2ButtonStats {
	
	private $_tableGateway;
	private $_adapter;
	private $_table = "s4_v2_button_stats";
	
	public function __construct($adapter){
		$this->_adapter = $adapter;
		$this->_tableGateway = new TableGateway($this->_table, $this->_adapter);
	}
	
	public function bulkInsert($data){
		foreach ($data as $ky=>$val){
			$insert = new Insert($this->_table);
			$insert->values($val);
			$this->_tableGateway->insertWith($insert);
		}
	}
}