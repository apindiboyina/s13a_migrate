<?php
namespace Application\Model;

class UrlUtil {
	
	public static function appendParam($url,$params){
		$urlArray = parse_url($url);
		$queryArray = array();
		parse_str($urlArray['query'],$queryArray);
		$params = array_merge($params,$queryArray);
		$query = http_build_query($params);
		if(!empty($queryArray['port']))
			return $urlArray['scheme']."://".$urlArray['host'].":".$urlArray['port'].$urlArray['path']."?".$query;
		
		return $urlArray['scheme']."://".$urlArray['host'].$urlArray['path']."?".$query;
	}
	
	public static function matchProtocol($src,$dest){
		$matches = array();
		$res = $dest;
		preg_match("/https?/", $src,$matches); //required for cross protocol messaging
		if(!empty($matches[0])){
			$proto = urldecode($matches[0]);
			$res = preg_replace("/https?/", $proto, $dest);
		}
		return $res;
	}
}