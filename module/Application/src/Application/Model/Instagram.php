<?php
namespace Application\Model;

use Zend\Serializer\Adapter\Json;
use Zend\Http\Client\Adapter\Curl;
use Zend\Http\Client;
use Zend\Http\Request;
use Application\Model\SocialLoginConfiguration;

class Instagram {
	
	private $_adapter;
	private $_clientID;
	private $_clientSecret;
	private $_redis = null;
	private $_siteid;
	private $_redirectUrl = "http://s13a.socialannex.net/application/instagram/redirect";
		
	public function __construct($siteid,$adapter,$redis=null){
		$this->_adapter = $adapter;
		$this->_siteid = $siteid;
		$this->_redis = $redis;
		$s13config = new S13V2Config($this->_adapter);
		$res = $s13config->getConfig($this->_siteid,$this->_redis);
		$this->_clientID = $res['instagram_client_id'];
		$this->_clientSecret = $res['instagram_secret'];
	}
	
	public function getWindowUrl($redirectUrl){
		
		$state = array (
				'siteid' => $this->_siteid,
				'url' => $redirectUrl
		);
		$stateStr = new Json();
		
		$params = array (
				'client_id' => $this->_clientID,
				'redirect_uri' => $this->_redirectUrl."?state=".$stateStr->serialize($state),
				'response_type' => "code",
				'scope'=>"basic comments relationships likes"
		);
		$authUrl = "https://api.instagram.com/oauth/authorize/?".http_build_query($params);
    return $authUrl;
	}
	
	public function updateUser($code,$state){
		$stateStr = new Json();
		$params = array (
				'code' => $code,
				'client_id' => $this->_clientID,
				'client_secret' => $this->_clientSecret,
				'redirect_uri' => $this->_redirectUrl."?state=".$stateStr->serialize($state),
				'grant_type' => "authorization_code"
		);
		$accessTokenUrl = "https://api.instagram.com/oauth/access_token";
		
		$curl = new Client($accessTokenUrl,array(
   		'adapter' => 'Zend\Http\Client\Adapter\Curl'
		));
		$curl->setParameterPost($params);
		$curl->setHeaders(array('Content-type'=>"application/x-www-form-urlencoded"));
		$curl->setMethod("POST");
		$accessToken = $curl->send();
		$accessToken = json_decode($accessToken->getBody());
		
		$suld = new SocialUserLoginDetails($this->_siteid, $this->_adapter);
		$name = explode(" ",$accessToken->user->full_name);
		$date = new \DateTime();
		$values = array(
			'site_id'=>$this->_siteid,
			'providers'=>"instagram",
			'loginprovideruid' => $accessToken->user->id,
			'user_name'=>$accessToken->user->username,
      'image_url'=>$accessToken->user->profile_picture,
      'firstname' => (isset($name[0]))?$name[0]:"",
      'lastname' => (isset($name[1]))?$name[1]:"",
			'db_update_date'=>$date->format(\DateTime::ATOM)
		);
		//check if email received
		$user = $suld->GetUserByLoginprovider('instagram', $values['loginprovideruid']);
		if($user && !empty($user['email'])){
			$values['email'] = $user['email'];
		}
		
		$values['id'] = $suld->UpdateUserInfo($values);
		return $values;
	}
}