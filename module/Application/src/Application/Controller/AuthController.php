<?php
namespace Application\Controller;

use Application\Model\S13V2Config;
use Application\Model\OAuthServer;
use Application\Model\SocialUserLoginDetails;
use Zend\Math\Rand;
use Application\Model\SocialLoginConfiguration;
class AuthController extends BaseController {
	
	public function getAccessTokenAction(){
		$code = $this->params()->fromQuery('code');
		$secret = $this->params()->fromQuery('client_secret');
		
		try{
			$storage = $this->_getRedis();
		}
		catch (\Exception $e){
			$storage = null;
		}
		
		$s13config = new S13V2Config($this->_adapter);
		$cfg = $s13config->getConfig($this->_siteid,$storage);
		if(boolval($cfg['enable_security']) == true && $secret != $cfg['client_secret_site'])
			throw new \Exception("Client not authenticated",403);
		
		
		$oauth = new OAuthServer($this->_siteid, $storage);
		$userid = $oauth->VerifyOauthCode($code);
		if(!$userid)
			throw new \Exception("Invalid OAuth Code", 401);
		$at = $oauth->GenerateAccessToken($userid);
		$this->_jv->setVariable('access_token', $at);
		$this->_jv->setVariable('expires', 3600);
		return $this->_jv;
	}
	
	public function getUserInfoAction(){
		$accessToken = $this->params()->fromQuery('access_token');
		try{
			$storage = $this->_getRedis();
		}	catch(\Exception $e){
			
		}
		
		$oauth = new OAuthServer($this->_siteid,$storage);
		$userid = $oauth->VerifyAccessToken($accessToken);
		if(!$userid)
			throw new \Exception("Invalid access token", 404);
		
		$profile = new SocialUserLoginDetails($this->_siteid, $this->_adapter);
		$profileData =$profile->GetUser($userid);
		//below login is just for old social login piggyback logic
		$slc = new SocialLoginConfiguration($this->_adapter);
		$random_id = uniqid (rand ());
		$timestamp = microtime(true);
		$profileData['sa_key']=$profileData['uid'].":".$profileData['siteuid'].":".$profileData['user_name'].":".$profileData['firstname'].":".$profileData['lastname'].":".$profileData['birth_date'].":".$profileData['gender'].":".$profileData['email'].":".$profileData['site_id'].":".$random_id.":".$timestamp.":".$profileData['providers'];
		//$profileData['sa_key'] = Rand::getString(5);
		$secret = $slc->getSecretForOldLogin($profileData['site_id']);
		$profileData['sa_token'] = rawurlencode(base64_encode(hash_hmac("sha256", $profileData['sa_key'], $secret, true ) ));
		
		$this->_jv->setVariables($profileData);
		return $this->_jv;
		
	}
}