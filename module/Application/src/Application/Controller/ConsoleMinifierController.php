<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\Client;
use Zend\Http\Client\Adapter\Curl;
class ConsoleMinifierController extends AbstractActionController{
	
	public function minifyjsAction(){
		$UNCOMPRESSED_FILE = __DIR__ ."/../../../../../public/js/s13-main.js";
		$COMPRESSED_FILE = __DIR__ ."/../../../../../public/js/s13-main.min.js";
		$js = file_get_contents($UNCOMPRESSED_FILE);
		
		$params = array(
				'js_code'=>$js,
				'compilation_level'=>"SIMPLE_OPTIMIZATIONS",
				'output_info'=>"compiled_code",
				'output_format'=>"text"
		);
		
		$closureUrl = "http://closure-compiler.appspot.com/compile";
		
		$curl = new Client($closureUrl);
		$curl->setAdapter(new Curl());
		$curl->setMethod("POST");
		$curl->setParameterPost($params);
		$response = $curl->send();
		file_put_contents($COMPRESSED_FILE, $response->getBody());
		
		echo "UnMinified file at :".$UNCOMPRESSED_FILE."\n";
		echo "Minified file at:".$COMPRESSED_FILE."\n";
	}
}