<?php
namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Application\Model\EmailVerification;
use Application\Model\SocialUserLoginDetails;
use Application\Model\PersonUnique;
class CollectEmailController extends BaseController{
	
	public function collectAction(){
		$loc = $this->params()->fromQuery('loc');
		$uid = $this->params()->fromQuery('uid');
		$vm = new ViewModel();
		$vm->setVariable('loc', $loc);
		$vm->setVariable('uid', $uid);
		$vm->setVariable('siteid', $this->_siteid);
		$vm->setTerminal(true);
		return $vm;
	}
	
	public function verifyEmailAction(){
		$request = $this->getRequest();
		$email = $request->getPost('email');//->fromQuery('email');
		$this->_siteid = $request->getPost('siteid');
		$ev = new EmailVerification($this->_siteid, $this->_adapter);
		$val = $ev->SendVerificationEmail($email);
		$this->_jv->setVariables($val);
		return $this->_jv;
	}
	
	public function verifyCodeAction(){
		$request = $this->getRequest();
		$email = $request->getPost('email');
		$this->_siteid = $request->getPost('siteid');
		$code = $request->getPost('code');
		$uid = $request->getPost('uid');
		$ev = new EmailVerification($this->_siteid, $this->_adapter);
		$val = $ev->CheckCode($code, $email);
		try{
			$redis = $this->_getRedis();
		}
		catch (\Exception $e){
			$redis = null;
		}
		if($val){
			$suld = new SocialUserLoginDetails($this->_siteid, $this->_adapter);
			$pu = new PersonUnique($this->_siteid, $this->_adapter, $redis);
			$puid = $pu->getPUID($email);
			$values = array(
				'email'=>$email,
				'pu_id'=>$puid['pu_id']
			);
			$suld->UpdateByID($uid, $values);
		}
		$this->_jv->setVariable('result', $val);
		return $this->_jv;
	}
}