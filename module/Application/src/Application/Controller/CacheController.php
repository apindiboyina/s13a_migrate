<?php
namespace Application\Controller;

class CacheController extends BaseController{
	
	public function clearAction(){
		if(!$this->_siteid)
			throw new \Exception("Siteid not provided", 400);
		
		$redis = $this->_getRedis();
		$res = $redis->del($this->_siteid);
		$this->_jv->setVariable('siteid', $this->_siteid);
		$this->_jv->setVariable('cleared', boolval($res));
		return $this->_jv;
	}
}