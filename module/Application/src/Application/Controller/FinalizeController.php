<?php
namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Application\Model\UrlUtil;
use Application\Model\MobileDetect;
class FinalizeController extends BaseController{
	
	public function windowRedirectAction(){
		$code = $this->params()->fromQuery('code');
		$iframeSrc = $this->params()->fromQuery('url');
		
		$iframeSrc = UrlUtil::appendParam($iframeSrc, array('s13code'=>$code));
		
		$md = new MobileDetect();
		if($md->isMobile()){
			$this->redirect()->toUrl($iframeSrc);
		}
		else {
			$vm = new ViewModel();
			$vm->setVariable('siteid', $this->_siteid);
			$vm->setVariable('iframesrc', $iframeSrc);
			$vm->setVariable("code", $code);
			$vm->setTerminal(true);
			return $vm;
		}
	}
}