<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\Plugin\Params;

class ConsoleRunnerController extends AbstractActionController{
	
	public function runS4ButtonsCronAction(){
		
		$command = "php public/index.php get";
		$dat = array();
		exec($command,$dat);
		$view = new JsonModel(array('command'=>$command,'resp'=>$dat));
		return $view;
	}
	
	public function setS4ButtonStatAction(){
		$siteid = $this->params()->fromQuery('siteid');
		$incid = $this->params()->fromQuery('incid');
		$ip = $this->params()->fromQuery('ip');
		$userid = $this->params()->fromQuery('userid');
		$command = "/usr/local/bin/php public/index.php setserve $siteid $incid $userid $ip >/dev/null &";
		exec($command);
		$view = new JsonModel(array('command'=>$command));
		return $view;
	}
	
	public function setS4ButtonStatDisplayAction(){
		$siteid = $this->params()->fromQuery('siteid');
		$incid = $this->params()->fromQuery('incid');
		$ip = $this->params()->fromQuery('ip');
		$userid = $this->params()->fromQuery('userid');
		$command = "/usr/local/bin/php public/index.php setdisplay $siteid $incid $userid $ip >/dev/null &";
		exec($command);
		$view = new JsonModel(array('command'=>$command));
		return $view;
	}
	
	public function testAction(){
		$out = array();
		exec("/usr/local/bin/php -v > /dev/null &");
		$view = new JsonModel(array('response'=>"server ok",'res'=>$out));
		return $view;
	}
}