<?php
namespace Application\Controller;

use Zend\Serializer\Adapter\Json;
use Application\Model\Facebook;
use Application\Model\OAuthServer;
use Zend\Mvc\Service\RouterFactory;
use Application\Model\UrlUtil;
use Application\Model\SocialLoginLog;
use Application\Model\PersonUnique;
use Zend\View\Model\ViewModel;
use Application\Model\Yahoo;

class YahooController extends BaseController{
	
	public function startLoginAction(){
		$url = $this->params()->fromQuery('url');
		try{
			$redis = $this->_getRedis();
		}catch (\Exception $e){
			$vm = new ViewModel();
			$vm->setTerminal(true);
			$vm->setTemplate('error/login');
			return $vm;
		}
		$yh = new Yahoo($this->_siteid, $this->_adapter,$redis);
		$res = $yh->GetRequestToken($url);
		$this->redirect()->toUrl($res);
	}
	
	public function redirectAction(){
		
		$error = $this->params()->fromQuery('error');
		if($error){
			$vm = new ViewModel();
			$vm->setTerminal(true);
			$vm->setTemplate('error/login');
			return $vm;
		}
		
		$oauthToken = $this->params()->fromQuery('oauth_token');
		$oauthVerifier = $this->params()->fromQuery('oauth_verifier');
		$state = $this->params()->fromQuery('state');
		$json = new Json();
		$state = $json->unserialize($state);
		$this->_siteid = $state['siteid'];
		$clientRedirectUrl = $state['url'];
		
		try{
			$redis = $this->_getRedis();
		} catch (\Exception $e){
			$vm = new ViewModel();
			$vm->setTerminal(true);
			$vm->setTemplate('error/login');
			return $vm;
		}
		$fb = new Yahoo($this->_siteid, $this->_adapter,$redis);
		$profile = $fb->updateUser($oauthToken,$oauthVerifier);
		
		//log the user login
		SocialLoginLog::Log($this->_adapter, $this->_siteid, $profile);
		$oauth = new OAuthServer($this->_siteid, $redis);
		$oauthCode = $oauth->GenerateOauthCode($profile['id']);
		
		//redirecting to s13b , because there is a Load Balancer bug,
		//the loadbalancer will not redirect from https to http on same domain
		$url = "http://s13a.socialannex.net/application/finalize/windowredirect?".http_build_query(array('siteid'=>$this->_siteid,'code'=>$oauthCode,'url'=>$clientRedirectUrl));
		$url = UrlUtil::matchProtocol($clientRedirectUrl,$url);
		
		if(!$profile['email']){
			$emailurl = "http://s13a.socialannex.net/application/collectemail/collect?".http_build_query(array('siteid'=>$this->_siteid,'loc'=>$url,'uid'=>$profile['id']));
			$this->redirect()->toUrl($emailurl);
		}
		else{
			$this->redirect()->toUrl($url);
		}
	}
}