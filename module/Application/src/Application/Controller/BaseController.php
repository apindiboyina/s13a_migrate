<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Db\Adapter\Adapter;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplatePathStack;
use Zend\View\Model\JsonModel;

class BaseController extends AbstractActionController{
	protected $_adapter;
	protected $_siteid;
	protected $_callback;
	protected $_renderer;
	protected $_jv;
	
	public function onDispatch(MvcEvent $e){
		$this->_callback = $this->params()->fromQuery('callback');
		$this->_siteid = $this->params()->fromQuery('siteid');
		$this->_siteid = intval($this->_siteid);
		$this->_adapter = new Adapter($this->getServiceLocator()->get('config')['db1']);
		
		$this->_renderer = new PhpRenderer();
		$resolver = new TemplatePathStack();
		$resolver->addPath(__DIR__.'/../../../view');
		$this->_renderer->setResolver($resolver);
		
		$this->_jv = new JsonModel();
		if($this->_callback)
			$this->_jv->setJsonpCallback($this->_callback);
		
		return parent::onDispatch($e);
	}
	
	protected function _getRedis(){
		$redis = new \Redis();
		$config = $this->getServiceLocator()->get('config');
		$redisConfig = $config['redisS13Instance'];
		if(!$redis->connect($redisConfig['host'],$redisConfig['port'],2)){
			throw new \Exception("unable to connect to redis server");
		}
		return $redis;
	}
}