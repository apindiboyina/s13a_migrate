<?php
namespace Application\Controller;

use Application\Model;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Application\Model\SocialLoginConfiguration;
use Application\Model\WindowUrls;


class InitController extends BaseController{
	
	public function getConfigAction(){
		
		$clientRedirectUrl = $this->params()->fromQuery('url');
		$data = array();
		try{
			$redis = $this->_getRedis();
			$val = $redis->hGet($this->_siteid,'config:'.$clientRedirectUrl); //the config key will be based on the url
			if(!$val)
				throw new \Exception("config not found in cache",500);
			$data = unserialize($val);
		}
		catch (\Exception $e){
			$s13config = new SocialLoginConfiguration($this->_adapter);
			$enabledProviders = $s13config->providersEnable($this->_siteid);
			$windowUrls = new WindowUrls($this->_siteid, $this->_adapter);
			$model = new ViewModel();
			$model->setVariable("result", $s13config->getConfig($this->_siteid));
			$model->setTemplate('application/index/smallhtml');
			$smallhtml  = $this->_renderer->render($model);
			
			$model->setTemplate('application/index/buttonhtml');
			$largehtml  = $this->_renderer->render($model);
			$data = array(
					'enable'=>$enabledProviders,
					'largehtml'=>$largehtml,
					'smallhtml'=>$smallhtml,
					'urls'=>$windowUrls->getWindowUrls($enabledProviders, $clientRedirectUrl)
			);
			
			try{
				$redis = $this->_getRedis();
				$redis->hset($this->_siteid,'config:'.$clientRedirectUrl,serialize($data));
			}
			catch (\Exception $e){
				
			}
		}
		$this->_jv->setVariables($data);
		return $this->_jv;
	}
}
