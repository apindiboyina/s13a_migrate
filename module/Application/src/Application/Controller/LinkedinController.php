<?php
namespace Application\Controller;

use Zend\Serializer\Adapter\Json;
use Application\Model\Facebook;
use Application\Model\OAuthServer;
use Zend\Mvc\Service\RouterFactory;
use Application\Model\UrlUtil;
use Application\Model\SocialLoginLog;
use Application\Model\PersonUnique;
use Application\Model\GooglePlus;
use Application\Model\Amazon;
use Zend\Filter\HtmlEntities;
use Application\Model\Paypal;
use Application\Model\Linkedin;
use Zend\View\Model\ViewModel;

class LinkedinController extends BaseController{
	
	public function redirectAction(){
		$error = $this->params()->fromQuery('error');
		if($error){
			$vm = new ViewModel();
			$vm->setTerminal(true);
			$vm->setTemplate('error/login');
			return $vm;
		}
		
		$code = $this->params()->fromQuery('code');
		$state = $this->params()->fromQuery('state');
		$json = new Json();
		$state = html_entity_decode($state);
		$state = $json->unserialize($state);
		$this->_siteid = $state['siteid'];
		$clientRedirectUrl = $state['url'];
		
		try{
			$redis = $this->_getRedis();
		} catch (\Exception $e){
			$redis = null;
		}
		$ln = new Linkedin($this->_siteid, $this->_adapter,$redis);
		$profile = $ln->updateUser($code);
		
		//log the user login
		SocialLoginLog::Log($this->_adapter, $this->_siteid, $profile);
		$oauth = new OAuthServer($this->_siteid, $redis);
		$oauthCode = $oauth->GenerateOauthCode($profile['id']);
		
		//redirecting to s13b , because there is a Load Balancer bug,
		//the loadbalancer will not redirect from https to http on same domain
		$url = "http://s13a.socialannex.net/application/finalize/windowredirect?".http_build_query(array('siteid'=>$this->_siteid,'code'=>$oauthCode,'url'=>$clientRedirectUrl));
		$url = UrlUtil::matchProtocol($clientRedirectUrl,$url);
		$this->redirect()->toUrl($url);
	}
}